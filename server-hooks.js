'use strict';
const eejs = require('ep_etherpad-lite/node/eejs');
const settings = require('ep_etherpad-lite/node/utils/Settings');

exports.eejsBlock_editbarMenuRight = (hookName, args, cb) => {
  args.content =
    eejs.require('ep_padgpt/templates/transcribe-button.ejs', {}, module) +
    args.content;
  return cb();
};

exports.eejsBlock_editorContainerBox = (hookName, args, cb) => {
  args.content += eejs.require(
      'ep_padgpt/templates/transcribe-popup.ejs',
      {langs: settings.ep_padgpt.availableLanguages},
      module,
  );
  return cb();
};

// Provide default settings (for dev mode)
exports.loadSettings = (hookName, args, cb) => {
  if (!settings.ep_padgpt) {
    settings.ep_padgpt = {
      availableLanguages: [
        {code: 'fr', name: 'Français', voskUrl: 'wss://localhost/fr'},
        {code: 'en', name: 'English', voskUrl: 'wss://localhost/en'},
      ],
    };
  }
  return cb();
};
// Pass plugin settings to server
exports.clientVars = async (hookName, args) => ({ep_padgpt: settings.ep_padgpt});
