# ep_padgpt

This plugin allows you to add live speech recognition to etherpad using vosk-server : your microphone audio is converted to text and directly inserted in a pad !

# Usage

- Click on the microphone icon (top right)
- Change language if needed
- Click on "Start speech recognition"
- Allow the browser to use your microphone
- See the text magically appearing : it's not magic, it's your voice
- Click on "Stop speech recognition" when you're done

# Installation

## Quick start with docker

Running the following commands (warning, it will use around 10GB of RAM because of vosk-server) will get you a running instance of etherpad with the plugin installed and everything needed at https://localhost :

```bash
openssl req -x509 -newkey rsa:4096 -keyout docker/nginx/key.pem -out docker/nginx/cert.pem -sha256 -days 3650 -nodes -subj "/C=XX/ST=StateName/L=CityName/O=CompanyName/OU=CompanySectionName/CN=CommonNameOrHostname"
cd docker
git clone https://github.com/ether/etherpad-lite
cp settings.json.docker etherpad-lite/
cp env.template .env
docker compose up -d
```

## Production

At the very least, you want to :

- Change the url to be publicly accessible
- Change your settings.json to point to your publicly accessible vosk-servers

## Manual installation

This plugin is not yet published on npm, so you'll need to install it from git :

```
npm install --no-save --legacy-peer-deps git+https://gitlab.cemea.org/mallette/ep_padgpt.git
```

Please note that while the plugin itself will install and run fine, you won't be able to use it unless you have a running vosk-server for the speech recognition part.

## Configuration

In `settings.json.docker` (or `settings.json` if you're running a regular install), you can add or remove languages with the following syntax :

```json
 "ep_padgpt": {
    "availableLanguages": [
      { "code": "fr", "name": "Français", "voskUrl": "wss://localhost/fr" },
      { "code": "en", "name": "English", "voskUrl": "wss://localhost/en" }
    ]
  }
```

The URLs of your vosk-server must be accessible for users, since it's the user browser (and not etherpad server) which will connect to them. For a public deployment, you'll probably need them accessible on the Internet.

# Development

You cannot use HTTP, since microphone API are restricted to secure context only. I recommend running the quick start with docker to get the vosk-server, then run etherpad in dev mode in your machine however you want (as long as you are using a reverse proxy with SSL).

I'm currently facing a bug where `npm install /path/to/plugin/folder` is not working (because it creates symlink and somehow it breaks etherpad's `require`), so you can run `install-dev.sh` which will watch for modifications on the file and copy them to etherpad folder (dumb, but it works)
