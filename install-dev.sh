#!/usr/bin/env bash


EXTENSION_FOLDER=$(pwd)
# Path to etherpad base repo
ETHERPAD_FOLDER=../etherpad-lite
inotifywait -e close_write,moved_to,create -m . -r |
while read -r directory events filename; do
    # don't copy hidden files
    rsync -av "$EXTENSION_FOLDER" "$ETHERPAD_FOLDER"/node_modules/ --exclude=".[!.]*" --exclude="install-dev.sh" --exclude="docker/" --exclude="node_modules/"
done