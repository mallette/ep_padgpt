'use strict';

// Reference code : https://github.com/alphacep/vosk-server/blob/master/client-samples/javascript/voice_client_with_audio_worklet.js
/*
  Responsible for :
  * Connecting to vosk via a Websocket
  * Setting up user audio
  * Getting raw user audio data
  * Sending this to vosk
  * Relaying results from Vosk to something (via callbacks)
*/
class VoskSender {
  constructor(onPartialResult, onResult) {
    this.ws = undefined;
    this.audioContext = undefined;
    this.currentPartial = '';
    this.onPartialResult = onPartialResult;
    this.onResult = onResult;

    this._isRunning = false;
  }

  async setupAudio(mediaStream) {
    // According to https://github.com/alphacep/vosk-server/issues/214 we might have to use webRTC.
    // But I don't know how to setup it properly, so I'll try this instead
    // To get raw audio, we need to use AudioContext
    // We'll pipe data through AudioContext from our source (user microphone) to an AudioNode for processing
    // Unfortunately, all of this is happening inside Web Audio render thread, which is fairly limited
    // In particular, there is no websocket support inside
    // So our AudioNode processing will be very limited : caching data to send it 4k bytes at a time using the integrated MessageChannel to relay raw audio back to VoskSender
    // VoskSender will be responsible for sending data to vosk, getting the result back, and updating the page with the data (maybe)
    // And in main thread, we'll actually send data to vosk, get the result back, and update the page with the data

    this.audioContext = new AudioContext();
    await this.audioContext.audioWorklet.addModule(
        '/static/plugins/ep_padgpt/static/js/data-conversion-processor.js'
    );
    const processor = new AudioWorkletNode(
        this.audioContext,
        'data-conversion-processor',
        {
          channelCount: 1,
          numberOfInputs: 1,
          numberOfOutputs: 1,
        }
    );

    // Let's connect our processor to the source
    const source = this.audioContext.createMediaStreamSource(mediaStream);
    source.connect(processor);

    // Not sure what this does
    processor.connect(this.audioContext.destination);
    // Finally, let's forward data
    processor.port.onmessage = (event) => this.processAudioData(event);
    processor.port.start();
  }

  async onWSConnect(mediaStream) {
    await this.setupAudio(mediaStream);
    this.sendSampleRate(this.audioContext.sampleRate);
    this._isRunning = true;
  }

  disconnectWS() {
    // We cannot await websocket closing, so we'll just forget about the object or we might end up reusing a closing websocket, depending on timing
    // Vosk-server is closing the connection when it gets the eof, so we won't have badly-closed connections hanging around
    this.ws.send('{"eof" : 1}');
    this.ws = null;
  }

  // Tell vosk server what our sample rate will be
  sendSampleRate(sampleRate) {
    if (!this.ws) {
      console.warn('Attempted to set sampleRate but WS is not connected');
      return;
    }
    const configMessage = {
      // eslint-disable-next-line camelcase
      config: {sample_rate: sampleRate},
    };
    console.debug(`Setting sampleRate to ${sampleRate}`);
    this.ws.send(JSON.stringify(configMessage));
  }

  /* Since we cannot await Websocket, we'll connect audio in ws.onopen, or we might risk a race condition where audio is setup and trying to send messages to ws before WS is connected)
   */
  startTranscribe(wsURL, mediaStream) {
    console.debug(`Starting transcribe to ${wsURL} with audio`, mediaStream);
    this.ws = new WebSocket(wsURL);

    this.ws.onopen = () => {
      this.onWSConnect(mediaStream);
    };
    this.ws.onerror = (event) => {
      console.error('WS ERROR : ', event);
    };
    this.ws.onmessage = (event) => this.processVoskMessage(event);
    this.ws.onclose = (event) => {
      console.debug('WS closed : ', event);
    };
  }

  async stopTranscribe() {
    if (this.ws) {
      this.disconnectWS();
    }

    await this.audioContext?.close();
    this._isRunning = false;
  }

  isRunning() {
    return this._isRunning;
  }

  processVoskMessage(event) {
    if (!event.data) {
      return;
    }
    const parsed = JSON.parse(event.data);
    // We get a bunch of partial messages ; Let's filter it so we don't get do processing multiple time the same partial
    if (parsed.partial && parsed.partial !== this.currentPartial) {
      const newPartial = parsed.partial;
      this.onPartialResult(this.currentPartial, newPartial);
      this.currentPartial = newPartial;
    } else if (parsed.result) {
      this.onResult(parsed.text, this.currentPartial);
      this.currentPartial = '';
    }
  }

  processAudioData(event) {
    if (this.ws?.readyState === WebSocket.OPEN) {
      this.ws.send(event.data);
    }
  }
}
exports.VoskSender = VoskSender;
