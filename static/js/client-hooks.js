'use strict';

const VoskSender = require('ep_padgpt/static/js/vosk-sender').VoskSender;
// TODO: probably not the best way to store this
let voskSender = null;
let ace = null;
// We must store the current options, so we can check on change() if something *actually* changed
const options = {
  lang: html10n.language,
  deviceId: undefined,
};

/* Try to get the vosk URL for a lang * */
const getVoskURL = (langCode) => {
  const out = clientVars.ep_padgpt?.availableLanguages.find(
      (x) => x.code === langCode
  )?.voskUrl;
  if (!out) {
    // Should never happen
    throw new Error(`Invalid lang for vosk : ${langCode}`);
  }
  return out;
};
/* Populate the audio input select with all availables devices
 */
const populateAudioInputSelect = async () => {
  const devices = await navigator.mediaDevices.enumerateDevices();
  const audioInput = $('#ep_padgpt-audioinput');
  audioInput.children().remove();
  for (const device of devices) {
    if (device.kind === 'audioinput') {
      audioInput.append(new Option(device.label, device.deviceId));
    }
  }

  // Etherpad does some strange stuff with select, so we have to do this to update GUI
  if ($('select').niceSelect) {
    $('select').niceSelect('update');
  }
};

const userMediaStream = async (deviceId) => {
  const constraints = {
    video: false,
    audio: {
      channelCount: 1,
      echoCancellation: true,
      noiseSuppression: true,
    },
  };
  if (deviceId) {
    constraints.audio.deviceId = deviceId;
  }
  return await navigator.mediaDevices.getUserMedia(constraints);
};

const onTranscribeLangChange = async (event) => {
  // Etherpad does some strange stuff with select, so we have to do this to update GUI
  if ($('select').niceSelect) {
    $('select').niceSelect('update');
  }
  const newLang = event.target.value;
  // If you click on the already selected value, it will still trigger a change so we need to filter those out
  if (newLang === options.lang) {
    return;
  }
  options.lang = newLang;

  // Restart with correct lang
  if (voskSender.isRunning()) {
    await voskSender.stopTranscribe();
    voskSender.startTranscribe(
        getVoskURL(options.lang),
        await userMediaStream(options.deviceId)
    );
  }
};

const onAudioInputChange = async (event) => {
  // Etherpad does some strange stuff with select, so we have to do this to update GUI
  if ($('select').niceSelect) {
    $('select').niceSelect('update');
  }
  const newDeviceId = event.target.value;
  // If you click on the already selected value, it will still trigger a change so we need to filter those out
  if (newDeviceId === options.deviceId) {
    return;
  }
  options.deviceId = newDeviceId;

  // Restart with correct audio input
  if (voskSender.isRunning()) {
    await voskSender.stopTranscribe();
    voskSender.startTranscribe(
        getVoskURL(options.lang),
        await userMediaStream(options.deviceId)
    );
  }
};

const onStartClick = async (event) => {
  const mediaStream = await userMediaStream(options.deviceId);
  // Now we need audioDeviceID to select the correct device in the input <select>
  const tracks = mediaStream?.getAudioTracks();
  if (!tracks || tracks.length !== 1) {
    throw Error('Invalid number of tracks, expected 1');
  }
  const deviceId = tracks[0].getSettings()?.deviceId;
  await populateAudioInputSelect();
  $('#ep_padgpt-audioinput').val(deviceId).trigger('change');
  voskSender.startTranscribe(getVoskURL(options.lang), mediaStream);

  $('#ep_padgpt-btn-stop').show();
  $('#ep_padgpt-btn-start').hide();
  $('#ep_padgpt-a-toggle button').css('color', 'red');
};

const onStopClick = (event) => {
  $('#ep_padgpt-btn-stop').hide();
  $('#ep_padgpt-btn-start').show();
  $('#ep_padgpt-a-toggle button').css('color', '');

  if (voskSender.isRunning()) {
    voskSender.stopTranscribe();
  }
};

const onMediaDeviceChange = async (event) => {
  const devices = await navigator.mediaDevices.enumerateDevices();
  await populateAudioInputSelect();
  if (!devices.find((element) => element.deviceId === options.deviceId)) {
    console.warn(
        'Active audio input is no longer usable, stopping transcription'
    );
    if (voskSender.isRunning()) {
      onStopClick();
      options.deviceId = undefined;
    }
  } else {
    // Just need to make sure we are still selecting the same audio input
    $('#ep_padgpt-audioinput').val(options.deviceId).trigger('change');
  }
};

/* Callback for vosk partial
 * Vosk sends partial result very frequently (multiple times per second), which are what vosk currently thinks the text is
 * Note: this is called only when the partial result actually changes, filtering identical partial is done in vosk.processVoskMessage
 * @param {string} currentPartial - The last partial
 * @param {string} newPartial - the new partial
 */
const onPartialResult = (currentPartial, newPartial) => {
  ace.callWithAce(
      (aceTop) => {
        const rep = aceTop.ace_getRep();
        // If we are not at start of line, add a space (so words aren't glued together)
        if (rep.selStart[1] !== 0) {
          newPartial = ` ${newPartial}`;
        }

        // Replace the actual text
        aceTop.ace_replaceRange(rep.selStart, rep.selEnd, newPartial);
        // Update selection - after inserting, selection is at end of insertion, so move backwards
        aceTop.ace_performSelectionChange(
            [rep.selEnd[0], rep.selEnd[1] - newPartial.length],
            rep.selEnd,
            true
        );
        // Show what the current partial is
        aceTop.ace_setAttributeOnSelection('underline', true);
      },
      'VoskPartialResult',
      true
  );
};

/* Callback for vosk result
 * Vosk sends results when it thinks a "phrase" is over (i.e it detected a pause in speaking)
 * @param {string} result - The final result from vosk ; it's lowercase text without punctuation
 * @param {string} currentPartial - the last partial result
 */
const onResult = (result, currentPartial) => {
  console.debug(`onResult ${result}`);
  ace.callWithAce(
      (aceTop) => {
        const rep = aceTop.ace_getRep();
        // If we are not at start of line, add a space (so words aren't glued together)
        if (rep.selStart[1] !== 0) {
          result = ` ${result}`;
        }
        // Remove underline
        aceTop.ace_setAttributeOnSelection('underline', false);
        // Replace the actual text
        aceTop.ace_replaceRange(rep.selStart, rep.selEnd, result);
      },
      'VoskResult',
      true
  );
};

/* init everything needed for the plugin : VoskSender and event handlers
 */
const init = (context) => {
  voskSender = new VoskSender(onPartialResult, onResult);
  ace = context.ace;

  $('#ep_padgpt-transcribe-lang').on('change', onTranscribeLangChange);
  $('#ep_padgpt-audioinput').on('change', onAudioInputChange);
  $('#ep_padgpt-btn-start').on('click', onStartClick);
  $('#ep_padgpt-btn-stop').on('click', onStopClick);

  navigator.mediaDevices.addEventListener('devicechange', onMediaDeviceChange);
  // Let's change language to etherpad's language
  // TODO: use a cookie or something to remember the value ?
  // Trigger manually because changes from javascript doesn't trigger change event in jquery
  $('#ep_padgpt-transcribe-lang').val(options.lang).trigger('change');
};

exports.postAceInit = (hook, context, cb) => {
  init(context);
  return cb();
};
exports.postToolbarInit = (hook, context, cb) => {
  context.toolbar.registerDropdownCommand('ep_padgpt-popup');
  return cb();
};
